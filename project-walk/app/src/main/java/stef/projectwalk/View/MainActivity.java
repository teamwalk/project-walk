package stef.projectwalk.View;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import stef.projectwalk.Model.ChartModel;
import stef.projectwalk.Controller.CountryFinder;
import stef.projectwalk.Model.GetData;
import stef.projectwalk.Model.Cache;
import stef.projectwalk.Controller.ParseJSON;
import stef.projectwalk.R;

/**
 * Unique activity of the program. Displays the activity_main.xml
 */
public class MainActivity extends AppCompatActivity {

    private CountryFinder countryFinder ;
    private ArrayList<ChartModel> chartModels;
    private ChartModel speciesModel;
    private ChartModel co2Model;
    private MultiSelectionSpinner countriesList;
    private ViewFlipper viewFlipper;
    private Legend barLegend;
    private Legend lineLegend;
    private TreeMap<String,String[]> dataOnCache = new TreeMap<>();


    /**
     * creates the main screen with the charts
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BarChart fishChart = (BarChart) findViewById(R.id.speciesChart);

        LineChart co2Chart = (LineChart) findViewById(R.id.co2Chart);

        LinearLayout background = (LinearLayout) findViewById(R.id.background);
        background.setBackgroundResource(R.drawable.greenfieldandarrow);



        viewFlipper = (ViewFlipper) findViewById(R.id.flipper);
        final GestureDetector gestureDetector = new GestureDetector(new MyGestureDetector());
        viewFlipper.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return !gestureDetector.onTouchEvent(event);
            }
        });

        createTables(fishChart, co2Chart);

        setUpCountriesList();

        try {
            dataOnCache = (TreeMap<String, String[]>) Cache.read(this);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     * checks internet connectivity when resuming the app
     * to check where to take information from (either cache or internet)
     */
    @Override
    protected void onResume() {
        super.onResume();
        if(!checkInternetAvailability()&&(dataOnCache.size()>0)){
            Log.i("cake159", (new ArrayList<String>(dataOnCache.keySet())).toString());
            countriesList.setItems(new ArrayList<String>(dataOnCache.keySet()));
        }else if(!checkInternetAvailability()){
            countriesList.setItems(new String[] {"no items in cache"});
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * creates the tables with the given barchart and linechart
     * @param barChart
     * @param lineChart
     */
    public void createTables(BarChart barChart, LineChart lineChart) {

        speciesModel = new ChartModel(barChart, "bar");
        co2Model = new ChartModel(lineChart, "line");
        chartModels = new ArrayList<>();

        chartModels.add(speciesModel);
        chartModels.add(speciesModel);
        chartModels.add(speciesModel);

        chartModels.add(co2Model);

        barLegend = barChart.getLegend();
        lineLegend = lineChart.getLegend();

    }

    /**
     * retrieves data either from internet or cached data
     */
    public void retrieveData() {
        for (ChartModel chartmodel : chartModels) {
            chartmodel.resetChart();
        }
        List<String> countries = countriesList.getSelectedStrings();
        TreeMap<String, Integer> legendMap = new TreeMap<>();
        for (String curCountry : countries) {

            Random r = new Random();
            r.nextInt();
            legendMap.put(curCountry, Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
            barLegend.setCustom(new ArrayList<Integer>(legendMap.values()), new ArrayList<String>(legendMap.keySet()));
            lineLegend.setCustom(new ArrayList<Integer>(legendMap.values()), new ArrayList<String>(legendMap.keySet()));
            speciesModel.legendMap = legendMap;
            co2Model.legendMap = legendMap;

            if (checkInternetAvailability() || dataOnCache.containsKey(curCountry)) {
                asyncFetch cake = new asyncFetch();
                cake.countryName = curCountry;
                cake.context = this;
                String country = countryFinder.getCountryList().get(curCountry);
                cake.execute(country, "EN.FSH.THRD.NO"
                        , "EN.HPT.THRD.NO"
                        , "EN.BIR.THRD.NO"
                        , "EN.ATM.CO2E.PC");
                try {
                    cake.get(3000, TimeUnit.MILLISECONDS);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * adds countries to MultiSelectionSpinner
     */
    private void setUpCountriesList() {
        countriesList = new MultiSelectionSpinner(this);
        countriesList = (MultiSelectionSpinner)findViewById(R.id.spinner);

        final List<String> list = new ArrayList<String>();
        //get from file
        countryFinder = new CountryFinder(getResources().openRawResource(R.raw.countries_to_code));

        //add countries to list
        for (String CountryName : countryFinder.getCountryList().keySet()) {
            list.add(CountryName);
        }
        //sort the list by name
        Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

        // Apply the adapter to the spinner
        countriesList.setItems(list);


    }

    /**
     * checks interenet connectivity
     * @return true if there is connectivity
     */
    public boolean checkInternetAvailability() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //connected to a network
            connected = true;
        }
        return connected;
    }

    /**
     * async method to fetch data from API and optimise format for the chartModel
     */
    private class asyncFetch extends AsyncTask<String, String, Void> {
        public String countryName;
        private Context context;
        @Override
        protected Void doInBackground(String... params) {
            String country = params[0];
            String[] jsonResults = new String[params.length];

            for (int i = 1; i < params.length; i++) {
                ParseJSON json;
                if (checkInternetAvailability()) {
                    GetData data = new GetData(country, params[i]);
                    jsonResults[i - 1] = data.getJsonResponse();
                    json = new ParseJSON(data.getJsonResponse());
                    if (dataOnCache == null) {
                        dataOnCache = new TreeMap<>();
                    }
                    if (!dataOnCache.containsKey(country)) {
                        dataOnCache.put(countryName, jsonResults);
                        Cache.write(context, dataOnCache);
                    }
                } else {
                    json = new ParseJSON(dataOnCache.get(countryName)[i - 1]);
                }

                chartModels.get(i - 1).curCountry = countryName;
                try {
                    if (!chartModels.get(i - 1).getChartType().equals("bar")) {
                        chartModels.get(i - 1).convertValuesToEntries(json.getInformation());
                    } else {
                        chartModels.get(i - 1).convertBarValues(i - 1, json.getInformation());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            for (ChartModel c: chartModels) {
                c.updateChart();
            }
        }
    }

    /**
     * Detect user action of horizonally swiping the screen
     */
    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            System.out.println(" in onFling() :: ");
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                return false;
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                viewFlipper.showNext();
            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                viewFlipper.showPrevious();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
