package stef.projectwalk.Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * connect app to the WORLD BANK API and receives data
 */
public class GetData {

    private String jsonResponse = "";


    /**
     * create request URL and retrieves data from the WORLD BANK API
     * @param whichCountry
     * @param whatData
     */
    public GetData(String whichCountry, String whatData) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;
            StringBuilder buffer = new StringBuilder();
            try {


                URL url = new URL("http://api.worldbank.org/countries/"
                        + whichCountry + "/indicators/" + whatData
                        + "?per_page=10000&date=1960:2015&format=json");

                connection = (HttpURLConnection) url.openConnection();
                //opens a connection to url
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                String line;
                // reads the data using a buffer from the stream which contains the remote data
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                this.jsonResponse = buffer.toString();

            } catch (IOException e) {
                e.printStackTrace();

            } finally {
                // tries to close all connections and buffers to stop memory leaks and future errors
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

    }

    /**
     * @return return data received from the World Bank API
     */
    public String getJsonResponse() {
        return jsonResponse;
    }


}
