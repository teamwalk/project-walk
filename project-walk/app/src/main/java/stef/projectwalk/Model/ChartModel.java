package stef.projectwalk.Model;

import android.util.Log;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Model class to add data to the charts.
 */
public class ChartModel {
    private LineData lineData;
    private ArrayList<LineDataSet> lineDataSets;
    private BarData barData;
    private ArrayList<BarDataSet> barDataSets;
    private String chartType;
    private Chart chart;
    private ArrayList<String> labelsOnTopChart;
    public TreeMap<String,Integer> legendMap = new TreeMap<>();
    public String curCountry;


    /**
     * Constructor that initialises the charts.
     * @param chart
     * @param chartType
     */
    public ChartModel(Chart chart, String chartType) {
        this.chartType = chartType;
        if (chartType.equals("bar")) {
            barDataSets = new ArrayList<>();
        } else {
            lineDataSets = new ArrayList<>();
        }
        labelsOnTopChart = new ArrayList<>();
        this.chart = chart;
    }

    /**
     * add data to the BarChart
     * @param labels (data indicators on top of the chart e.g. fish, plants, birds)
     * @param dataSet (data to be displayed on the chart)
     */
    public void addDataToBarChart(ArrayList<String> labels, BarDataSet dataSet) {
        barDataSets.add(dataSet);
        for (String s : labels) {
            if (!this.labelsOnTopChart.contains(s)) {
                this.labelsOnTopChart.add(s);
            }
        }

    }

    /**
     * add data to the LineChart
     * @param labels (years labelsOnTopChart displayed on top of the chart)
     * @param dataSet (data to be displayed on hte chart)
     */
    public void addDataToLineChart(ArrayList<String> labels, LineDataSet dataSet) {
        lineDataSets.add(dataSet);
        for (String s : labels) {
            if (!this.labelsOnTopChart.contains(s)) {
                this.labelsOnTopChart.add(s);
            }
        }
    }

    /**
     * sets Data to the charts
     */
    public void setData() {
        if (chartType.equals("bar")) {
            barData = new BarData(labelsOnTopChart, barDataSets);
            chart.setData(barData);
            chart.setDescription("");


        } else {
            lineData = new LineData(labelsOnTopChart, lineDataSets);
            lineData.setDrawValues(false);
            chart.setData(lineData);
            chart.setDescription("");


        }
    }

    /**
     * convert a treeMap of values into entryData for LineChart
     * @param jsonMap
     */
    public void convertValuesToEntries(TreeMap<Integer, Double> jsonMap)
    {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();

        int k = 0;

        for(int i = 1960;i<2015 ; i++)
        {
            if(jsonMap.get(i) == null)
            {
                continue;
            }

            labels.add(String.valueOf(i));
            entries.add(new Entry (jsonMap.get(i).floatValue(),k));
            k++;
        }

        convertEntriesToDataSets(entries, labels);
    }

    /**
     * converts the treeMap into entryData for BarChart
     * @param indicatorCounter (int used how many times the method has been called)
     * @param dataFromIndicators (data in json format)
     */
    public void convertBarValues(int indicatorCounter, TreeMap<Integer, Double> dataFromIndicators) {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();
        labels.add("Birds");
        labels.add("Plants");
        labels.add("Fish");
        try{
            if (indicatorCounter == 0) {
                entries.add(new Entry(dataFromIndicators.get(2014).floatValue(), 0));
            } else if (indicatorCounter == 1) {

                entries.add(new Entry(dataFromIndicators.get(2014).floatValue(), 1));
            } else if (indicatorCounter == 2) {

                entries.add(new Entry(dataFromIndicators.get(2014).floatValue(), 2));
            }
        } catch (NullPointerException e) {
            if (indicatorCounter == 0) {

                entries.add(new Entry(0, 0));
            } else if (indicatorCounter == 1) {

                entries.add(new Entry(0, 1));
            } else if (indicatorCounter == 2) {

                entries.add(new Entry(0, 2));
            }
        }

        convertEntriesToDataSets(entries, labels);
    }

    /**
     * convert the entries of labelsOnTopChart into bar or line dataSets.
     * @param entries (data)
     * @param labels (indicators/years on top of the chart)
     */
    public void convertEntriesToDataSets(ArrayList<Entry> entries, ArrayList<String> labels) {
        if (getChartType().equals("bar")) {
            ArrayList<BarEntry> barEntries = new ArrayList<>();
            for (Entry e : entries) {
                barEntries.add(new BarEntry(e.getVal(), e.getXIndex()));
            }
            BarDataSet dset = new BarDataSet(barEntries, "");

            dset.setColor(legendMap.get(curCountry));

            addDataToBarChart(labels, dset);
        } else {
            LineDataSet dset = new LineDataSet(entries, "");
            dset.setDrawCircles(false);
            dset.setColor(legendMap.get(curCountry));
            addDataToLineChart(labels, dset);
        }
    }

    /**
     * @return chartType of chart (line or bar)
     */
    public String getChartType() {
        return chartType;
    }

    /**
     * @return chart
     */
    public Chart getChart() {
        return chart;
    }

    /**
     * display the data on the actual chart with animation
     */
    public void updateChart(){
        setData();
        getChart().animateY(2500);
    }

    /**
     * empty the chart from the current data
     */
    public void resetChart(){
        if (!getChart().isEmpty()){
            if (chartType.equals("bar")) {
                barDataSets = new ArrayList<>();
            } else {
                lineDataSets = new ArrayList<>();
            }
        }
    }
}