package stef.projectwalk.Model;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Read or write information from the cached files ofthe app.
 */
public class Cache {

    /**
     * reads information from the cached files.
     * @param context
     * @return
     */
    public static Object read(Context context) {
        try {
            FileInputStream fileInputStream = context.openFileInput(context.getCacheDir().getName());
            ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
            Object toRead = inputStream.readObject();
            inputStream.close();
            fileInputStream.close();
            return toRead;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * passes information to be written to the cache memory and writes it into the local storage.
     * @param context
     * @param toWrite (information to write into cache)
     */
    public static void write(Context context, Object toWrite){
        try {
            File cacheFile = new File(context.getFilesDir().getPath()+"/cache");
            cacheFile.createNewFile();
            FileOutputStream fileOutputStream = context.openFileOutput(context.getCacheDir().getName(), Context.MODE_PRIVATE);
            ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
            outputStream.writeObject(toWrite);
            outputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
