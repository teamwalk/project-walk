package stef.projectwalk.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Creates an object that reads a csv file and stores
 * a list of countries in a map, in order to be used later.
 */
public class CountryFinder {
    private HashMap<String,String> countriesCodesMap;
    private InputStream inputFile ;

    /**
     * creates a new CountryFinder and takes the countries from the file passed as a parameter
     * @param inputFile
     */
    public CountryFinder(InputStream inputFile) {
        countriesCodesMap = new HashMap<>();
        this.inputFile = inputFile;
        readCSVCountryFile();
    }
    

    /**
     * read the csv file and store data in a field map.
     */
    public void readCSVCountryFile() {

        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(inputFile));
            String line;

            while ((line = buffer.readLine()) != null) {
                String[] RowData = line.split(",");
                countriesCodesMap.put(RowData[0], RowData[1]);
            }
            buffer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return map that contains name of the countries (keys) and their acronyms (values)
     */
    public HashMap<String,String> getCountryList() {
        return countriesCodesMap;
    }

}
