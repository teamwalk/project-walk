package stef.projectwalk.Controller;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.TreeMap;

/**
 * Receives json response from WORLD BANK API and changes it in a readable format for the charts
 */
public class ParseJSON {

    private JSONArray jsonArray;
    private TreeMap<Integer, Double> information;

    /**
     * constructor takes data from API and cleans it up by calling parseArray()
     * @param json (response from API)
     */
    public ParseJSON(String json) {
        if (json.startsWith("[")) {
            try {
                this.jsonArray = new JSONArray(json);
                jsonArray = jsonArray.optJSONArray(1);
                if (jsonArray == null) {
                    return;
                }
                information = new TreeMap<Integer, Double>();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            parseArray();
        }
    }


    /**
     * takes the json objects inside the json array returned by the WORLD BANK API
     * that contain relevant information for the charts
     */
    public void parseArray() {
        try {
            int year = 2015;
            for (int i = 0; i < jsonArray.length(); i++) {
                if (!jsonArray.getJSONObject(i).isNull("value")) {
                    if (!jsonArray.getJSONObject(i).optString("value").equals("null") && !jsonArray.getJSONObject(i).optString("value").equals("")) {
                        information.put(year, Double.valueOf(jsonArray.getJSONObject(i).optString("value")));
                    }
                }
                --year;
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    /**
     * @return cleaned information (years and values) from the WORLD BANK API
     */
    public TreeMap<Integer, Double> getInformation() {
        return information;
    }
}
