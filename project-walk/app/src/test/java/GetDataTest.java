import org.junit.Assert;
import org.junit.Test;

import stef.projectwalk.Model.GetData;

public class GetDataTest {

    private GetData getData;

    @Test
    public void setUp() {
        String country = "Austria";
        String indicator = "EN.HPT.THRD.NO";

        getData = new GetData(country,indicator);

        String json = getData.getJsonResponse();

        Assert.assertNotNull(getData);
        Assert.assertEquals(json.charAt(0),'[');
        Assert.assertEquals(json.charAt(json.length()-1),']');

    }

}
