
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static junit.framework.Assert.*;

import java.io.InputStream;

import stef.projectwalk.Controller.CountryFinder;
import stef.projectwalk.View.MainActivity;
import stef.projectwalk.R;

public class CountryFinderTest {


   MainActivity mainActivity = Mockito.mock(MainActivity.class);

    CountryFinder countryFinder;

    @Before
    public void setUp() {
        InputStream resourceCountries = mainActivity.getResources().openRawResource(R.raw.countries_to_code);

        countryFinder = new CountryFinder(resourceCountries);
    }

    @Test
    public void firstCountryExpectedValueTest () {
        String firstCountry = "Afghanistan";
        String firstAcronym = "AFG";

        assertEquals(countryFinder.getCountryList().keySet().toArray()[0], firstCountry);
    }

}
